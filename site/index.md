
# GuayaHack

## Grupo de Estudio y [Hackerspace](https://en.wikipedia.org/wiki/Hackerspace)

Aquí, sin importar el nivel, nos gusta aprender sobre programación, informática al igual que todo tema y tecnología relevante en el mundo moderno. GuayaHack es un espacio creado por y para la {doc}`/community/index` el cual surgió como idea en [/r/Colombia](https://www.reddit.com/r/Colombia/comments/151fkiz/con_una_prima_y_un_amigo_armaremos_un_grupo_de). `GuayaHack` nació el `16 de julio de 2023` a las `20:00:00 CEST`, marcando el inicio de ésta aventura, [únete en Discord](https://discord.gg/trzuezGrZd).

```console
$ date=`date -d @1689530400`; echo -e "\nGuayaHack, fundado: $date"

GuayaHack, fundado: So 16. Jul 20:00:00 CEST 2023
```

```{div} discord-widget
<iframe src="https://discord.com/widget?id=1130256195345727560&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
```

## Sobre GuayaHack

*GuayaHack* es un juego de palabras entre *Guayacán*, el nombre coloquial del árbol [Handroanthus chrysanthus](https://en.wikipedia.org/wiki/Handroanthus_chrysanthus), y el término [Hacker](https://es.wikipedia.org/wiki/Hacker), utilizado para referirse a entusiastas de la tecnología. Inclusive tenemos un poema inspirado en ésta curiosa combinación titulado {doc}`/community/poema-cronicas-guayahack`.

Puedes leer más sobre GuayaHack y sus miembros en nuestro {doc}`/community/memorial`, echándole un vistazo las [Reglas](community/rules.md) o en la página de la {doc}`/community/index`. 


## ¿Quién puede Participar?

%TODO:write las descripciones de los niveles y añadir ésta linea
%{doc}`/wiki/organizacion-nivel-novato`, {doc}`/wiki/organizacion-nivel-experimentado` y {doc}`/wiki/organizacion-nivel-profesional`

Únete usando [éste link de invitación](https://discord.gg/trzuezGrZd) para entrar a nuestro Discord y sigue los pasos en `#rules`.

Sin importar si eres principiante, tienes experiencia o eres profesional, comenzarás resolviendo {doc}`/reto/on-boarding-git-gitlab` y luego decidirás que camino quieres tomar.

```{toctree}
:maxdepth: 1
:hidden:
:caption: "# GuayaHack"
# Memorial <community/memorial.md>
# Noticias <noticias.md>
# Calendario <calendario.md>
# Posts <posts/index.md>
# Reglas <community/rules.md>
# Comunidad <community/index.md>
# WIKI <https://guayahack.co/posts/category/wiki/>
# ¿Cómo Ayudo? <https://gitlab.com/guayahack/main/-/issues/>
```

## Guías, Materiales y Otros

Todo en la [#WIKI](https://guayahack.co/posts/category/wiki/), siempre en la [#WIKI](https://guayahack.co/posts/category/wiki/) :D

## Destacado

```{postlist}
:tags: destacado
```

## Lo Último en GuayaHack

```{postlist} 10
```


